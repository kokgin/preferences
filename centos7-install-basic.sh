#!/usr/bin/env bash

echo "--- script begin ---"

echo "--- change hostname ---"
hostname dev-server

echo "--- update the system ---"
yum update -y

echo "--- install EPEL repo ---"
yum install -y epel-release

echo "--- install basic tools ---"
yum install -y vim wget

echo "--- add alias for frequent used commands ---"
cat << EOF | tee -a ~/.bashrc
# Frequent used
alias c="clear"
EOF
# Command below not working
#. ~/.bashrc

echo "--- script end ---"

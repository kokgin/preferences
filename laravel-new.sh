#!/usr/bin/env bash

echo "--- setup laravel site ---"

cd /var/www/

echo -e "Please enter project name: "
read project_name
laravel new $project_name

chmod -R 777 $project_name/storage/
chcon -R -t httpd_sys_rw_content_t $project_name/storage/

chmod -R 777 $project_name/bootstrap/cache/
chcon -R -t httpd_sys_rw_content_t $project_name/bootstrap/cache/

echo "<VirtualHost *:80>
    ServerName $project_name.dev
    DocumentRoot /var/www/$project_name/public
    ProxyPassMatch ^/(.*\.php(/.*)?)$ fcgi://127.0.0.1:9000/var/www/$project_name/public/$1
    DirectoryIndex /index.php index.php

    <Directory /var/www/$project_name/public/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>" > /etc/httpd/conf.d/$project_name.conf

systemctl reload httpd.service

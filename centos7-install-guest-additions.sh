#!/usr/bin/env bash

echo "--- script begin ---"

# Mount GuestAdditions to guest OS via VirtualBox
# Click the Devices > Insert Guest Additions CD image option while a virtual machine is running
# http://www.if-not-true-then-false.com/2010/install-virtualbox-guest-additions-on-fedora-centos-red-hat-rhel/

echo "--- update the system ---"
yum update -y

echo "--- install necessary packages ---"
yum install -y dkms gcc make kernel-devel kernel-headers bzip2 perl

echo "--- script end ---"

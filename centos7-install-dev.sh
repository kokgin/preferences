#!/usr/bin/env bash

echo "--- script begin ---"

echo "--- change hostname ---"
hostname dev-server

echo "--- update the system ---"
yum update -y

echo "--- install EPEL repo ---"
yum install -y epel-release

echo "--- install basic tools ---"
yum install -y vim wget

echo "--- install IUS repo (for php5.6) ---"
wget https://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-14.ius.centos7.noarch.rpm
rpm -Uvh ius-release*.rpm
rm -f ius-release*.rpm

echo "--- install Apache ---"
yum install -y httpd
systemctl start httpd.service
systemctl enable httpd.service
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload
sed -i "s/^LoadModule mpm_prefork_module modules\/mod_mpm_prefork.so$/#LoadModule mpm_prefork_module modules\/mod_mpm_prefork.so/" /etc/httpd/conf.modules.d/00-mpm.conf
sed -i "s/^#LoadModule mpm_event_module modules\/mod_mpm_event.so$/LoadModule mpm_event_module modules\/mod_mpm_event.so/" /etc/httpd/conf.modules.d/00-mpm.conf
systemctl restart httpd.service
setsebool -P httpd_can_network_connect=1
setsebool -P httpd_enable_homedirs=1
setsebool -P httpd_anon_write=1
setsebool -P httpd_can_sendmail=1

echo "--- install PHP-FPM 5.6 ---"
yum install -y php56u-fpm php56u-gd php56u-mbstring php56u-mcrypt php56u-mysqlnd php56u-pdo php56u-intl
sed -i "s/^;date.timezone =$/date.timezone = \"Asia\/Kuala_Lumpur\"/" /etc/php.ini
echo "--- for development server ---"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php.ini
sed -i "s/display_startup_errors = .*/display_startup_errors = On/" /etc/php.ini
sed -i "s/log_errors_max_len = .*/log_errors_max_len = 0/" /etc/php.ini
sed -i "s/track_errors = .*/track_errors = On/" /etc/php.ini
sed -i "s/session.gc_maxlifetime = .*/session.gc_maxlifetime = 7200/" /etc/php.ini
systemctl start php-fpm.service
systemctl enable php-fpm.service

echo "--- install php-xdebug (development server only) ---"
yum install -y php56u-pecl-xdebug
cat << EOF | tee -a /etc/php.d/15-xdebug.ini
xdebug.scream=1
xdebug.cli_color=1
xdebug.show_local_vars=1
EOF
systemctl reload php-fpm.service

echo "--- install Git ---"
yum install -y git

echo "--- install Composer ---"
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

echo "--- install Laravel 5.1 ---"
composer global require "laravel/installer=~1.1"
echo 'pathmunge ~/.composer/vendor/bin' > /etc/profile.d/composer_vendor.sh
chmod +x /etc/profile.d/composer_vendor.sh
# Command below not working
# . /etc/profile

echo "--- install MariaDB ---"
echo "# MariaDB 10.0 CentOS repository list - created 2015-07-03 01:39 UTC
# http://mariadb.org/mariadb/repositories/
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.0/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1" > /etc/yum.repos.d/MariaDB.repo
yum install -y MariaDB-server MariaDB-client
systemctl start mysql.service
systemctl enable mysql.service

echo "--- install phpMyAdmin ---"
cd /var/www/
wget https://files.phpmyadmin.net/phpMyAdmin/4.4.10/phpMyAdmin-4.4.10-all-languages.tar.gz
tar -zxvf phpMyAdmin-4.4.10-all-languages.tar.gz
cd phpMyAdmin-4.4.10-all-languages
mv config.sample.inc.php config.inc.php
echo "--- for development server ---"
sed -i "s/?>//" /var/www/phpMyAdmin-4.4.10-all-languages/config.inc.php
cat << EOF | tee -a /var/www/phpMyAdmin-4.4.10-all-languages/config.inc.php
\$cfg['LoginCookieValidity'] = 7200;
EOF
echo "<VirtualHost *:80>
    ServerName pma.dev
    DocumentRoot /var/www/phpMyAdmin-4.4.10-all-languages
    ProxyPassMatch ^/(.*\.php(/.*)?)$ fcgi://127.0.0.1:9000/var/www/phpMyAdmin-4.4.10-all-languages/$1
    DirectoryIndex /index.php index.php

    <Directory /var/www/phpMyAdmin-4.4.10-all-languages/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>" > /etc/httpd/conf.d/phpmyadmin.conf
systemctl reload httpd.service
rm -f /var/www/phpMyAdmin-4.4.10-all-languages.tar.gz

echo "--- install nodejs ---"
yum install -y nodejs

echo "--- install npm ---"
yum install -y npm

echo "--- install gulp via npm ---"
npm install -g gulp

echo "--- install dependencies of gulp-notify ---"
yum install -y libnotify
# npm install --save-dev gulp-notify
# npm install --save-dev notify-send
# npm install --save-dev node-notifier

echo "--- script end ---"

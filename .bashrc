# Frequent used
alias cdw="cd ~/code/; pwd"
alias ll="ls -l"
alias eth0="nmcli dev list iface eth0"
#alias mdev="mount -t vboxsf test /var/www/test"
#alias mdev="sshfs root@127.0.0.1:/var/www ~/code/dev-server -p 2222"
#alias umdev="umount /var/www/test"
#alias sdev="ssh root@127.0.0.1 -p 2222"
#alias pu="php -c ~/tools/php/php.ini ~/tools/php/vendor/bin/phpunit --test-suffix ".php" --colors $@"
#alias puf="php -c ~/tools/php/php.ini ~/tools/php/vendor/bin/phpunit --stop-on-failure --test-suffix ".php" --colors $@"
#alias phpcs="php ~/tools/php/vendor/bin/phpcs --standard=PSR2 $@"
# Git stuff
. ~/.git-prompt.sh
export PS1='\[\e[1;32m\]\u@\h\[\e[0;49;94m\] \w\[\e[1;33m\]$(__git_ps1)\[\e[1;34m\] $\[\e[1;00m\] ';
# Docker
alias dk="docker $@"
alias dkp="docker pull $@"
alias dkr="docker run $@"
alias dkb="docker build $@"
alias dkc="docker-compose $@"
# Laravel Artisan
alias pa="php artisan $@"
alias pami="php artisan migrate"
alias pamiro="php artisan migrate:rollback"
alias pamire="php artisan migrate:refresh"
alias pammi="php artisan make:migration $@"
alias pammo="php artisan make:model $@"
alias pamco="php artisan make:controller $@"
